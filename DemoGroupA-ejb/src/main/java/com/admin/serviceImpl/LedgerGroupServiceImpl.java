/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.serviceImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.AdminDao;
import com.admin.dao.LedgerGroupDao;
import com.admin.dao.StatusDao;
import com.admin.dto.CollegeDto;
import com.admin.dto.LedgerGroupDto;
import com.admin.mapper.LedgerGroupMapper;
import com.admin.service.LedgerGroupService;
import com.payrollSystem.entity.common.LedgerGroup;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author shahk_000
 */
@Stateless
public class LedgerGroupServiceImpl implements LedgerGroupService {
    @EJB
    private LedgerGroupDao ledgerGroupDao;
    
    @EJB
    private StatusDao statusDao;

    @EJB
    private AdminDao adminDao;
    
     @Override
    public boolean save(LedgerGroupDto ledgerGroupDto) {
        return ledgerGroupDao.save(convertToLedgerGroup(ledgerGroupDto));
    }
    
    private LedgerGroup convertToLedgerGroup(LedgerGroupDto ledgerGroupDto) {
        LedgerGroup ledgerGroup = new LedgerGroup();
        ledgerGroup.setCreatedByAdmin(adminDao.getById(ledgerGroupDto.getCreatedByAdminDto().getId()));
        ledgerGroup.setCreatedDate(new Date());
        ledgerGroup.setName(ledgerGroupDto.getName());
        ledgerGroup.setCode(ledgerGroupDto.getCode());
        ledgerGroup.setDescription(ledgerGroupDto.getDescription());
        ledgerGroup.setMainGroup(ledgerGroupDto.getMainGroup());
        
        ledgerGroup.setStatus(statusDao.getByDesc(StatusConstants.CREATE_APPROVE.getName()));
        return ledgerGroup;
    }
    
    private void setCreateEditCommonParameters(LedgerGroup ledgerGroup, LedgerGroupDto ledgerGroupDto) {
        ledgerGroup.setName(ledgerGroupDto.getName());
        ledgerGroup.setCode(ledgerGroupDto.getCode());
        ledgerGroup.setDescription(ledgerGroupDto.getDescription());
        ledgerGroup.setMainGroup(ledgerGroupDto.getMainGroup());
    }
    
     @Override
    public boolean delete(LedgerGroupDto ledgerGroupDto) {
        LedgerGroup ledgerGroup = ledgerGroupDao.getById(ledgerGroupDto.getId());
        ledgerGroup.setDeletedDate(new Date());
        ledgerGroup.setDeletedReason(ledgerGroupDto.getDeletedReason());
        ledgerGroup.setDeletedByAdmin(adminDao.getById(ledgerGroupDto.getDeletedByAdminDto().getId()));
        ledgerGroup.setStatus(statusDao.getByDesc(StatusConstants.DELETED_APPROVE.getName()));
        return ledgerGroupDao.modify(ledgerGroup);
    }
    
     @Override
    public boolean update(LedgerGroupDto ledgerGroupDto) {
        LedgerGroup ledgerGroup = ledgerGroupDao.getById(ledgerGroupDto.getId());
        ledgerGroup.setLastUpdatedDate(new Date());
        ledgerGroup.setUpdatedByAdmin(adminDao.getById(ledgerGroupDto.getUpdatedByAdminDto().getId()));
        ledgerGroup.setStatus(statusDao.getByDesc(StatusConstants.EDIT_APPROVE.getName()));
        setCreateEditCommonParameters(ledgerGroup, ledgerGroupDto);
        return ledgerGroupDao.modify(ledgerGroup);
    }
    
     @Override
    public boolean checkIfLedgerGroupNameAlreadyExists(LedgerGroupDto ledgerGroupDto) {
        return ledgerGroupDao.checkIfLedgerGroupNameAlreadyExists(ledgerGroupDto);
    }
    
     @Override
    public boolean checkIfLedgerGroupCodeAlreadyExists(LedgerGroupDto ledgerGroupDto) {
        return ledgerGroupDao.checkIfLedgerGroupCodeALreadyExists(ledgerGroupDto);
    }
    
     @Override
    public List<LedgerGroupDto> findByCollegeId(CollegeDto collegeDto) {
        return LedgerGroupMapper.convertToDtos(ledgerGroupDao.findAllByCollegeId(collegeDto));
    }
    
    @Override
    public List<LedgerGroupDto> findByCollegeIdForDropDown(CollegeDto collegeDto) {
        return LedgerGroupMapper.convertToDtosForDropDown(ledgerGroupDao.findAllByCollegeId(collegeDto));
    }
     
}
