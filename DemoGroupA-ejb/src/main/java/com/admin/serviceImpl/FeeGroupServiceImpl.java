/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.serviceImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.AdminDao;
import com.admin.dao.FeeGroupDao;
import com.admin.dao.StatusDao;
import com.admin.dto.CollegeDto;
import com.admin.dto.FeeGroupDto;
import com.admin.mapper.FeeGroupMapper;
import com.admin.service.FeeGroupService;
import com.payrollSystem.entity.common.FeeGroup;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author shahk_000
 */
@Stateless
public class FeeGroupServiceImpl implements FeeGroupService {
    
     @EJB
    private FeeGroupDao feeGroupDao;

    @EJB
    private StatusDao statusDao;

    @EJB
    private AdminDao adminDao;
    
    @Override
    public boolean save(FeeGroupDto feeGroupDto) {
        return feeGroupDao.save(convertToFeeGroup(feeGroupDto));
    }
    
    private FeeGroup convertToFeeGroup(FeeGroupDto feeGroupDto) {
        FeeGroup feeGroup = new FeeGroup();
        feeGroup.setCreatedByAdmin(adminDao.getById(feeGroupDto.getCreatedByAdminDto().getId()));
        feeGroup.setCreatedDate(new Date());
        feeGroup.setName(feeGroupDto.getName());
        feeGroup.setCode(feeGroupDto.getCode());
        feeGroup.setDescription(feeGroupDto.getDescription());
        feeGroup.setActive(true);
        
        feeGroup.setStatus(statusDao.getByDesc(StatusConstants.CREATE_APPROVE.getName()));
        return feeGroup;
    }
    
     private void setCreateEditCommonParameters(FeeGroup feeGroup, FeeGroupDto feeGroupDto) {
        feeGroup.setDescription(feeGroupDto.getDescription());
        feeGroup.setName(feeGroupDto.getName());
        feeGroup.setCode(feeGroupDto.getCode());
        feeGroup.setActive(true);
                
    }

     @Override
    public boolean delete(FeeGroupDto feeGroupDto) {
        FeeGroup feeGroup = feeGroupDao.getById(feeGroupDto.getId());
        feeGroup.setDeletedDate(new Date());
        feeGroup.setDeletedReason(feeGroupDto.getDeletedReason());
        feeGroup.setDeletedByAdmin(adminDao.getById(feeGroupDto.getDeletedByAdminDto().getId()));
        feeGroup.setStatus(statusDao.getByDesc(StatusConstants.DELETED_APPROVE.getName()));
        return feeGroupDao.modify(feeGroup);
    }
     
     @Override
    public boolean update(FeeGroupDto feeGroupDto) {
        FeeGroup feeGroup = feeGroupDao.getById(feeGroupDto.getId());
        feeGroup.setLastUpdatedDate(new Date());
        feeGroup.setUpdatedByAdmin(adminDao.getById(feeGroupDto.getUpdatedByAdminDto().getId()));
        feeGroup.setStatus(statusDao.getByDesc(StatusConstants.EDIT_APPROVE.getName()));
        setCreateEditCommonParameters(feeGroup, feeGroupDto);
        return feeGroupDao.modify(feeGroup);
    }
    
     @Override
    public boolean checkIfFeeGroupNameAlreadyExists(FeeGroupDto feeGroupDto) {
        return feeGroupDao.checkIfFeeGroupNameAlreadyExists(feeGroupDto);
    }
    
     @Override
    public boolean checkIfFeeGroupCodeAlreadyExists(FeeGroupDto feeGroupDto) {
        return feeGroupDao.checkIfFeeGroupCodeAlreadyExists(feeGroupDto);
    }
    
     @Override
    public List<FeeGroupDto> findByCollegeId(CollegeDto collegeDto) {
        return FeeGroupMapper.convertToDtos(feeGroupDao.findAllByCollegeId(collegeDto));
    }
    
    @Override
    public List<FeeGroupDto> findByCollegeIdForDropDown(CollegeDto collegeDto) {
        return FeeGroupMapper.convertToDtosForDropDown(feeGroupDao.findAllByCollegeId(collegeDto));
    }
    
}
