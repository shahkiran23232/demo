/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.service;

import com.admin.dto.CollegeDto;
import com.admin.dto.LedgerGroupDto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author shahk_000
 */
@Local
public interface LedgerGroupService {
    
    public boolean save(LedgerGroupDto ledgerGroupdto);

    boolean delete(LedgerGroupDto ledgerGroupdto);
    
    boolean checkIfLedgerGroupNameAlreadyExists(LedgerGroupDto ledgerGroupdto);

    boolean checkIfLedgerGroupCodeAlreadyExists(LedgerGroupDto ledgerGroupdto);
    
    boolean update(LedgerGroupDto ledgerGroupdto);
    
    List<LedgerGroupDto> findByCollegeId(CollegeDto collegeDto);
    
    List<LedgerGroupDto> findByCollegeIdForDropDown(CollegeDto collegeDto);
    
}
