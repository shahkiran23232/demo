/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.service;

import com.admin.dto.BillingPeriodDto;
import com.admin.dto.CollegeDto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author shahk_000
 */
@Local
public interface BillingPeriodService {
    
    public boolean save(BillingPeriodDto billingPeriodDto);

    boolean delete(BillingPeriodDto billingPeriodDto);
    
    boolean checkIfBillingPeriodNameAlreadyExists(BillingPeriodDto billingPeriodDto);

    boolean checkIfBillingPeriodCodeAlreadyExists(BillingPeriodDto billingPeriodDto);
    
    boolean update(BillingPeriodDto billingPeriodDto);
    
    List<BillingPeriodDto> findByCollegeId(CollegeDto collegeDto);
    
    List<BillingPeriodDto> findByCollegeIdForDropDown(CollegeDto collegeDto);
    
}