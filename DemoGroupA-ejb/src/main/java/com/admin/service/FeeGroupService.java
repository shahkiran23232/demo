/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.service;

import com.admin.dto.CollegeDto;
import com.admin.dto.FeeGroupDto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author shahk_000
 */
@Local
public interface FeeGroupService {
    
    public boolean save(FeeGroupDto feeGroupdto);

    boolean delete(FeeGroupDto feeGroupdto);
    
    boolean checkIfFeeGroupNameAlreadyExists(FeeGroupDto feeGroupdto);

    boolean checkIfFeeGroupCodeAlreadyExists(FeeGroupDto feeGroupdto);
    
    boolean update(FeeGroupDto feeGroupdto);
    
    List<FeeGroupDto> findByCollegeId(CollegeDto collegeDto);
    
    List<FeeGroupDto> findByCollegeIdForDropDown(CollegeDto collegeDto);
    
}

