/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.constant;

/**
 *
 * @author shahk_000
 */
public enum LedgerGroupMainConstants {
    
    INCOME("Income"),
    EXPENDITURE("Expenditure"),
    ASSESTS("Assests"),
    LIABILITIES("Liabilities");
    
    private final String name;

     LedgerGroupMainConstants(String name) {
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
}
