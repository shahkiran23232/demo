/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.daoImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.FeeGroupDao;
import com.admin.dto.FeeGroupDto;
import com.payrollSystem.entity.common.FeeGroup;
import javax.ejb.Stateless;
import javax.persistence.Query;
/**
 *
 * @author shahk_000
 */
@Stateless
public class FeeGroupDaoImpl extends StatusableDaoImpl<FeeGroup> implements FeeGroupDao{
    
    public FeeGroupDaoImpl() {
        super(FeeGroup.class);
    }
    
    @Override
    public boolean checkIfFeeGroupNameAlreadyExists(FeeGroupDto feeGroupDto) {
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM FeeGroup s WHERE s.createdByAdmin.college.id=:collegeId AND s.name=:feeGroupName AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (feeGroupDto.getId() != null) {
            stringBuilder.append("and s.id<>:feeGroupId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", feeGroupDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("feeGroupName", feeGroupDto.getName());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        if (feeGroupDto.getId() != null) {
            query.setParameter("feeGroupId", feeGroupDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    }
    
    
    @Override
    public boolean checkIfFeeGroupCodeAlreadyExists(FeeGroupDto feeGroupDto) {
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM FeeGroup s WHERE s.createdByAdmin.college.id=:collegeId AND s.code=:feeGroupCode AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (feeGroupDto.getId() != null) {
            stringBuilder.append("and s.id<>:feeGroupId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", feeGroupDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        query.setParameter("feeGroupCode", feeGroupDto.getCode());
        if (feeGroupDto.getId() != null) {
            query.setParameter("feeGroupId", feeGroupDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    }
}
