/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.mapper;

import com.admin.dto.BillingPeriodDto;
import com.payrollSystem.entity.common.BillingPeriod;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author shahk_000
 */
public class BillingPeriodMapper extends AbstractCodeMapper{
    
    public static BillingPeriodDto convertToDto(BillingPeriod billingPeriod) {
        BillingPeriodDto billingPeriodDto = new BillingPeriodDto();
          billingPeriodDto.setHierarchy(billingPeriod.getHierarchy());
          billingPeriodDto.setActive(true);
        convertCommon(billingPeriodDto, billingPeriod);
        return billingPeriodDto;
    }

    public static List<BillingPeriodDto> convertToDtos(List<BillingPeriod> billingPeriods) {
        List<BillingPeriodDto> billingPeriodDtos = new ArrayList<>();
        for (BillingPeriod billingPeriod : billingPeriods) {
            billingPeriodDtos.add(convertToDto(billingPeriod));
        }
        return billingPeriodDtos;
    }

    public static BillingPeriodDto convertToDtoForDropDown(BillingPeriod billingPeriod) {
        BillingPeriodDto billingPeriodDto = new BillingPeriodDto();
        billingPeriodDto.setCode(billingPeriod.getCode());
        billingPeriodDto.setId(billingPeriod.getId());
        billingPeriodDto.setName(billingPeriod.getName());
        return billingPeriodDto;
    }
    
    public static List<BillingPeriodDto> convertToDtosForDropDown(List<BillingPeriod> billingPeriods) {
        List<BillingPeriodDto> billingPeriodDtos = new ArrayList<>();
        for (BillingPeriod billingPeriod : billingPeriods) {
            billingPeriodDtos.add(convertToDtoForDropDown(billingPeriod));
        }
        return billingPeriodDtos;
    }
    
}