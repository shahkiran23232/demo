/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.mapper;

import com.admin.dto.LedgerGroupDto;
import com.payrollSystem.entity.common.LedgerGroup;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author shahk_000
 */
public class LedgerGroupMapper extends AbstractCodeMapper {
    
    public static LedgerGroupDto convertToDto(LedgerGroup ledgerGroup) {
        LedgerGroupDto ledgerGroupDto = new LedgerGroupDto();
        ledgerGroupDto.setMainGroup(ledgerGroup.getMainGroup());
          
        convertCommon(ledgerGroupDto, ledgerGroup);
        return ledgerGroupDto;
    }
    
    public static List<LedgerGroupDto> convertToDtos(List<LedgerGroup> ledgerGroups) {
        List<LedgerGroupDto> ledgerGroupDtos = new ArrayList<>();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            ledgerGroupDtos.add(convertToDto(ledgerGroup));
        }
        return ledgerGroupDtos;
    }
    
    public static LedgerGroupDto convertToDtoForDropDown(LedgerGroup ledgerGroup) {
        LedgerGroupDto ledgerGroupDto = new LedgerGroupDto();
        ledgerGroupDto.setCode(ledgerGroup.getCode());
        ledgerGroupDto.setId(ledgerGroup.getId());
        ledgerGroupDto.setName(ledgerGroup.getName());
        return ledgerGroupDto;
    }
    
    public static List<LedgerGroupDto> convertToDtosForDropDown(List<LedgerGroup> ledgerGroups) {
        List<LedgerGroupDto> ledgerGroupDtos = new ArrayList<>();
        for (LedgerGroup ledgerGroup : ledgerGroups) {
            ledgerGroupDtos.add(convertToDtoForDropDown(ledgerGroup));
        }
        return ledgerGroupDtos;
    }
}
