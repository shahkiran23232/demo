/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.feeGroup;

import com.admin.dto.FeeGroupDto;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@ManagedBean
@SessionScoped

public class FeeGroupDataModelJsf implements Serializable {
    
    private FeeGroupDto feeGroupDto;
    private boolean createEditPanel;
    private List<FeeGroupDto> feeGroupDtos;
    
    public FeeGroupDto getFeeGroupDto() {
        if (feeGroupDto == null) {
            feeGroupDto = new FeeGroupDto();
        }
        return feeGroupDto;
    } 
    
}
