/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.feeGroup;

import com.admin.dto.AdminDto;
import com.admin.dto.CollegeDto;
import com.admin.dto.FeeGroupDto;
import com.admin.service.FeeGroupService;
import com.admin.util.Utility;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@ManagedBean
@RequestScoped
public class FeeGroupBeanJsf {
    
    @ManagedProperty(value = "#{feeGroupDataModelJsf}")
    private FeeGroupDataModelJsf feeGroupDataModelJsf;
    
     @EJB
    private FeeGroupService feeGroupService;
    
    private CollegeDto collegeDto;

    private AdminDto adminDto;
    
    @PostConstruct
    public void init() {
        collegeDto = new CollegeDto();
        collegeDto.setId(1l);

        adminDto = new AdminDto();
        adminDto.setId(1L);

        adminDto.setCollegeDto(collegeDto);
    }
    
    public String returnToPage() {
       return "feeGroup.xhtml?faces-redirect=true";
    }
    
    public String initCreate() {
        feeGroupDataModelJsf.setFeeGroupDto(new FeeGroupDto());
        feeGroupDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
      
    public String saveUpdate() {
        feeGroupDataModelJsf.getFeeGroupDto().setUpdatedByAdminDto(adminDto);
        feeGroupDataModelJsf.getFeeGroupDto().setCreatedByAdminDto(adminDto);
        
        if (feeGroupService.checkIfFeeGroupNameAlreadyExists(feeGroupDataModelJsf.getFeeGroupDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "FeeGroup Name Already Exists", null));
            return returnToPage();
        }
        if (feeGroupService.checkIfFeeGroupCodeAlreadyExists(feeGroupDataModelJsf.getFeeGroupDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "FeeGroup Code Already Exists", null));
            return returnToPage();
        }

        if (feeGroupDataModelJsf.getFeeGroupDto().getId() == null) {
            return save();
        } else {
            return update();
        }
    }
    
    private String save() {
        boolean response = feeGroupService.save(feeGroupDataModelJsf.getFeeGroupDto());
        if (response) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Saved", null));
        }
        return navigateToPage();
    }
    
    private String update() {
        boolean success = feeGroupService.update(feeGroupDataModelJsf.getFeeGroupDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updated Successfully", null));
        }
        return navigateToPage();
    }
        
    public String navigateToPage() {
        Utility.removeSessionBeanJSFDataModelObject("feeGroupDataModelJsf");
        feeGroupDataModelJsf = (FeeGroupDataModelJsf) Utility.getSessionObject("feeGroupDataModelJsf");
        feeGroupDataModelJsf.setFeeGroupDtos(feeGroupService.findByCollegeId(collegeDto));
        return returnToPage();
    }
         
    public String initEdit() {
        feeGroupDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
    
    public String delete() {
        feeGroupDataModelJsf.getFeeGroupDto().setDeletedByAdminDto(adminDto);
        
        boolean success = feeGroupService.delete(feeGroupDataModelJsf.getFeeGroupDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Deleted Successfully", null));
        }
        return navigateToPage();
    }
}

