/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.billingPeriod;

import com.admin.dto.AdminDto;
import com.admin.dto.BillingPeriodDto;
import com.admin.dto.CollegeDto;
import com.admin.service.BillingPeriodService;
import com.admin.util.Utility;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@ManagedBean
@RequestScoped
public class BillingPeriodBeanJsf {
    
     @ManagedProperty(value = "#{billingPeriodDataModelJsf}")
    private BillingPeriodDataModelJsf billingPeriodDataModelJsf;
    
    @EJB
    private BillingPeriodService billingPeriodService;
     
    private CollegeDto collegeDto;

    private AdminDto adminDto;
    
    @PostConstruct
    public void init() {
        collegeDto = new CollegeDto();
        collegeDto.setId(1l);

        adminDto = new AdminDto();
        adminDto.setId(1L);

        adminDto.setCollegeDto(collegeDto);
    }
    
    
    public String returnToPage() {
        return "billingPeriod.xhtml?faces-redirect=true";
    }
    
     public String initCreate() {
        billingPeriodDataModelJsf.setBillingPeriodDto(new BillingPeriodDto());
        billingPeriodDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
    
     public String saveUpdate() {
        billingPeriodDataModelJsf.getBillingPeriodDto().setUpdatedByAdminDto(adminDto);
        billingPeriodDataModelJsf.getBillingPeriodDto().setCreatedByAdminDto(adminDto);
        
        if (billingPeriodService.checkIfBillingPeriodNameAlreadyExists(billingPeriodDataModelJsf.getBillingPeriodDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "BillingPeriod Name Already Exists", null));
            return returnToPage();
        }
        if (billingPeriodService.checkIfBillingPeriodCodeAlreadyExists(billingPeriodDataModelJsf.getBillingPeriodDto())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "BillingPeriod Code Already Exists", null));
            return returnToPage();
        }

        if (billingPeriodDataModelJsf.getBillingPeriodDto().getId() == null) {
            return save();
        } else {
            return update();
        }
    }
     
     
     private String save() {
        boolean response = billingPeriodService.save(billingPeriodDataModelJsf.getBillingPeriodDto());
        if (response) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Saved", null));
        }
        return navigateToPage();
    }
     
      private String update() {
        boolean success = billingPeriodService.update(billingPeriodDataModelJsf.getBillingPeriodDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updated Successfully", null));
        }
        return navigateToPage();
    }
      
       public String navigateToPage() {
        Utility.removeSessionBeanJSFDataModelObject("billingPeriodDataModelJsf");
        billingPeriodDataModelJsf = (BillingPeriodDataModelJsf) Utility.getSessionObject("billingPeriodDataModelJsf");
        billingPeriodDataModelJsf.setBillingPeriodDtos(billingPeriodService.findByCollegeId(collegeDto));
        return returnToPage();
    }
       
       public String initEdit() {
        billingPeriodDataModelJsf.setCreateEditPanel(true);
        return returnToPage();
    }
        public String delete() {
        billingPeriodDataModelJsf.getBillingPeriodDto().setDeletedByAdminDto(adminDto);
        
        boolean success = billingPeriodService.delete(billingPeriodDataModelJsf.getBillingPeriodDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Deleted Successfully", null));
        }
        return navigateToPage();
    }
    
}