/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.billingPeriod;

import com.admin.dto.BillingPeriodDto;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@ManagedBean
@SessionScoped

public class BillingPeriodDataModelJsf implements Serializable {
    private BillingPeriodDto billingPeriodDto;
    private boolean createEditPanel;
    private List<BillingPeriodDto> billingPeriodDtos;

    public BillingPeriodDto getBillingPeriodDto() {
        if (billingPeriodDto == null) {
            billingPeriodDto = new BillingPeriodDto();
        }
        return billingPeriodDto;
    }   
}
