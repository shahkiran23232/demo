/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.ledgerGroup;

import com.admin.constant.LedgerGroupMainConstants;
import com.admin.dto.LedgerGroupDto;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@ManagedBean
@SessionScoped
public class LedgerGroupDataModelJsf implements Serializable{
    
     private LedgerGroupDto ledgerGroupDto;
    private boolean createEditPanel;
    private List<LedgerGroupDto> ledgerGroupDtos;


    public LedgerGroupDto getLedgerGroupDto() {
        if (ledgerGroupDto == null) {
            ledgerGroupDto = new LedgerGroupDto();
        }
        return ledgerGroupDto;
    }   
    
   
}
