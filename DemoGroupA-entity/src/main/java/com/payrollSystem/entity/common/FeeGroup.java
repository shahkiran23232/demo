/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@Entity
@Table(name="FEE_GROUP")
@NamedQueries({
   @NamedQuery(name = "FeeGroup.findAll", query = "SELECT fg FROM FeeGroup fg"),
   @NamedQuery(name = "FeeGroup.findByFeeGroupId", query = " SELECT fg FROM FeeGroup fg WHERE fg.id = :id")
})
public class FeeGroup extends AbstractCode{

    @Column(name ="IS_ACTIVE", nullable = false)
    private boolean isActive=true;
}
