/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
/**
 *
 * @author shahk_000
 */
@Getter
@Setter
@Entity
@Table(name = "LEDGER_GROUP")
@NamedQueries({
    @NamedQuery(name = "LedgerGroup.findAll", query = "SELECT lg FROM LedgerGroup lg"),
    @NamedQuery(name = "LedgerGroup.findByLedgerGroupId", query = "SELECT lg FROM LedgerGroup lg WHERE lg.id = :id")})

public class LedgerGroup extends AbstractCode {
    
    @Column(name = "MAIN_GROUP", nullable = false)
   
    private String mainGroup;
}
